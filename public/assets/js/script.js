for (let count = 1; count <= 1000; count++){
    if (count % 5 == 0){
        console.log(count);
    } else {
        continue;
    }
}

/* SOLUTION 1:

for (let count = 1; count <= 1000; count++){
    if (count % 5 == 0){
        console.log(count);
    }
}
 
 */



/* SOLUTION 2

let whileCounter = 1;
while(whileCounter <= 1000){
    if(whileCounter % 5 == 1000){
        console.log(i)
    }
}


 */



/* SOLUTION 3

let doWhileCounter = 1;
do {
    if(doWhileCounter % 5 == 1000){
        console.log(doWhileCounter);
    }
    doWhileCounter++;
} while (doWhileCounter <= 1000);

 */